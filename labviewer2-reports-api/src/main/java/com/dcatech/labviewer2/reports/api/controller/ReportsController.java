package com.dcatech.labviewer2.reports.api.controller;

import com.dcatech.commons.logs.logs.Filelogs;
import com.dcatech.labviewer2.reports.api.dto.RequestExportDto;
import com.dcatech.labviewer2.reports.service.impl.ReportsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;

@RestController
@RequestMapping("export")
public class ReportsController {

    @Autowired
    private ReportsServiceImpl reportsService;

    @PostMapping("/pdf")
    public ResponseEntity<byte[]> exportpdf(@RequestBody RequestExportDto paramreports) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        LinkedHashMap<String, Object> parames = new LinkedHashMap<>();
        String reportname = paramreports.getReportName();
        try {
            paramreports.getParameters().stream().forEach(
                    objectMap -> parames.putAll(objectMap)
            );
            //parames.put("mode", "pdfbytes");
           // parames.put("jasperprint", "pdf");
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }

        return reportsService.exportPdfFile(reportname,parames);
    }

    @GetMapping("/xlsx")
    public ResponseEntity<byte[]> exportexcel(@RequestBody RequestExportDto paramreports) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        LinkedHashMap<String, Object> parames = new LinkedHashMap<>();
        String reportname = paramreports.getReportName();
        try {
            paramreports.getParameters().stream().forEach(
                    objectMap -> parames.putAll(objectMap)
            );
            parames.put("mode", "xlsx");
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }
        return reportsService.exportXlsxFile(reportname,parames);
    }

    @GetMapping("/docx")
    public ResponseEntity<byte[]> exportdoc(@RequestBody RequestExportDto paramreports) throws SQLException, JRException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        LinkedHashMap<String, Object> parames = new LinkedHashMap<>();
        String reportname = paramreports.getReportName();
        try {
            paramreports.getParameters().stream().forEach(
                    objectMap -> parames.putAll(objectMap)
            );
        }catch (Exception e)
        {
            Filelogs.registerFilelogs(e.getMessage(),ReportsController.class,3);
            throw new NullPointerException(e.getMessage() + " ; Error creando el Map para el reporte" );
        }
        return reportsService.exportDocxFile(reportname,parames);
    }
}