package com.dcatech.labviewer2.reports.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RequestExportDto implements Serializable {

    private static final long serialVersionUID = 6143776116478098594L;
    private String reportName;
    private List<LinkedHashMap<String, Object>> parameters;
}
