package com.dcatech.labviewer2.reports.dao.impl;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;
import net.sf.jasperreports.engine.JRException;

import javax.sql.DataSource;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;

import com.dcatech.commons.exception.exceptions.FileNotFoundException;

@Repository
public class ReportsRepositoryJPA {


    @Value("${labvantage.report.path.icp}")
    private String ReportPath= null;


    @Autowired
    private DataSource dataSource;

    @Autowired
    private ResourceLoader resourceLoader;


    public JasperPrint generateReport(String reportname, LinkedHashMap<String, Object> paramreports) throws SQLException, IOException, JRException {
        String ruta = ReportPath  +  reportname + ".jasper" ;

        JasperPrint print ;
        JasperReport jasperReport = null;
        try {

            jasperReport = (JasperReport) JRLoader.loadObjectFromFile(ruta);
            print = JasperFillManager.fillReport(jasperReport, paramreports, dataSource.getConnection());

        }
        catch (JRException e){
            throw  new FileNotFoundException( e.getMessage() + " ; " + FileNotFoundException.DESCRIPTION);
        }
        return print;
    }


}