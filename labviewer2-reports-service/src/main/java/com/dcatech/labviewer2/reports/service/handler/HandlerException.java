package com.dcatech.labviewer2.reports.service.handler;


import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.dcatech.commons.exception.entity.ErrorMessage;

import com.dcatech.commons.exception.exceptions.NumberFormatException;
import com.dcatech.commons.exception.exceptions.NullPointerException;
import com.dcatech.commons.exception.exceptions.InstantiationException;
import com.dcatech.commons.exception.exceptions.IllegalAccessException;
import com.dcatech.commons.exception.exceptions.EnumConstantNotPresentException;
import com.dcatech.commons.exception.exceptions.ClassNotFoundException;
import com.dcatech.commons.exception.exceptions.ClassCastException;
import com.dcatech.commons.exception.exceptions.ArrayStoreException;
import com.dcatech.commons.exception.exceptions.ArithmeticException;
import com.dcatech.commons.exception.exceptions.NotFoundUserIdException;
import com.dcatech.commons.exception.exceptions.SQLTimeoutException;
import com.dcatech.commons.exception.exceptions.SQLSyntaxErrorException;
import com.dcatech.commons.exception.exceptions.SQLDataException;
import com.dcatech.commons.exception.exceptions.ServiceNotFoundException;
import com.dcatech.commons.exception.exceptions.RecordNotFoundException;
import com.dcatech.commons.exception.exceptions.NotDirectoryException;
import com.dcatech.commons.exception.exceptions.MissingFormatArgumentException;
import com.dcatech.commons.exception.exceptions.HttpRetryException;
import com.dcatech.commons.exception.exceptions.FailedLoginException;
import com.dcatech.commons.exception.exceptions.DateTimeException;
import com.dcatech.commons.exception.exceptions.AccountExpiredException;
import com.dcatech.commons.exception.exceptions.AccessDeniedException;
import com.dcatech.commons.exception.exceptions.URISyntaxException;
import com.dcatech.commons.exception.exceptions.PermissionDeniedException;
import com.dcatech.commons.exception.exceptions.TimeoutException;
import com.dcatech.commons.exception.exceptions.FileSystemException;
import com.dcatech.commons.exception.exceptions.FileNotFoundException;
import com.dcatech.commons.exception.exceptions.FIleDataFormatException;
import com.dcatech.commons.exception.exceptions.EventException;
import com.dcatech.commons.exception.exceptions.ConnectException;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

@ControllerAdvice
public class HandlerException extends ResponseEntityExceptionHandler {

    String msg = "";

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({SQLTimeoutException.class,SQLSyntaxErrorException.class,SQLDataException.class,
            ServiceNotFoundException.class,RecordNotFoundException.class, NumberFormatException.class,
            NullPointerException.class, NotFoundUserIdException.class, NotDirectoryException.class,
            MissingFormatArgumentException.class, HttpRetryException.class, FailedLoginException.class,
            DateTimeException.class, ClassCastException.class, ArrayStoreException.class, ArithmeticException.class,
            AccountExpiredException.class, AccessDeniedException.class})
    @ResponseBody
    public ErrorMessage BadRequest (HttpServletRequest request, Exception exception) throws  Exception{

        msg = exception.getMessage();
        String[] valores = msg.split(";");
        ErrorMessage manageErrors = new ErrorMessage();
        manageErrors.setStatus(HttpStatus.BAD_REQUEST.value());
        manageErrors.setError(valores[1]);
        manageErrors.setMessage(valores[0]);
        manageErrors.setRequest(request.getRequestURI());
        manageErrors.setMethod(request.getMethod());
        return manageErrors;

    }


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({TimeoutException.class, URISyntaxException.class,InstantiationException.class, IllegalAccessException.class,
            FileSystemException.class, FileNotFoundException.class, FIleDataFormatException.class, EventException.class,
            EnumConstantNotPresentException.class, ConnectException.class, ClassNotFoundException.class})
    @ResponseBody
    public ErrorMessage NotFoundRequest (HttpServletRequest request, Exception exception){

        msg = exception.getMessage();
        String[] valores = msg.split(";");
        ErrorMessage manageErrors = new ErrorMessage();
        manageErrors.setStatus(HttpStatus.BAD_REQUEST.value());
        manageErrors.setError(valores[1]);
        manageErrors.setMessage(valores[0]);
        manageErrors.setRequest(request.getRequestURI());
        manageErrors.setMethod(request.getMethod());
        return manageErrors;

    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler({PermissionDeniedException.class})
    @ResponseBody
    public ErrorMessage FordiddenRequest (HttpServletRequest request, Exception exception){

        msg = exception.getMessage();
        String[] valores = msg.split(";");
        ErrorMessage manageErrors = new ErrorMessage();
        manageErrors.setStatus(HttpStatus.BAD_REQUEST.value());
        manageErrors.setError(valores[1]);
        manageErrors.setMessage(valores[0]);
        manageErrors.setRequest(request.getRequestURI());
        manageErrors.setMethod(request.getMethod());
        return manageErrors;

    }
}
