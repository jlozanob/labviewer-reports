package com.dcatech.labviewer2.reports.service;

import net.sf.jasperreports.engine.JRException;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedHashMap;

public interface IReportsService {

    ResponseEntity<byte[]> exportPdfFile(String reportname, LinkedHashMap<String, Object> paramreports) throws SQLException, JRException, IOException;
    ResponseEntity<byte[]> exportXlsxFile(String reportname,LinkedHashMap<String, Object> paramreports) throws SQLException, JRException, IOException;
    ResponseEntity<byte[]> exportDocxFile(String reportname,LinkedHashMap<String, Object> paramreports) throws SQLException, JRException, IOException;

}
