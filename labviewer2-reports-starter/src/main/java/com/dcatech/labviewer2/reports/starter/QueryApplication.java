package com.dcatech.labviewer2.reports.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.dcatech.labviewer2.reports")
@EnableJpaRepositories("com.dcatech.labviewer2.reports.dao")
@EntityScan("com.dcatech.labviewer2.reports.model")
@ComponentScan(basePackages = {"com.dcatech.labviewer2.reports.api", "com.dcatech.labviewer2.reports.service", "com.dcatech.labviewer2.reports.dao"})
public class QueryApplication {

    public static void main(String[] args) {

        SpringApplication.run(QueryApplication.class, args);

    }
}
